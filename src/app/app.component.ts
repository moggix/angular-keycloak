import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'rainbow-web';

  constructor() {
    // console.log(`${environment.url}`);
    // console.log(`${environment.prop}`);
    // console.log(`${environment.token}`);
  }

  public updateTitle(): string {
    return 'Hola world ';
  }
}
